package de.upb.swarmrobotics;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import de.upb.swarmrobotics.core.Environment;
import de.upb.swarmrobotics.gui.BasicStage;
import de.upb.swarmrobotics.sheet3.*;
import de.upb.swarmrobotics.utils.Constants;
import de.upb.swarmrobotics.utils.Resources;

public class Main extends ApplicationAdapter {
	private Environment environment;
	private BasicStage stage;

	@Override
	public void create () {
		Resources.init();
		final int width = Constants.MIN_WIDTH / Constants.PIXELS_PER_METER;
		final int height = Constants.MIN_HEIGHT / Constants.PIXELS_PER_METER;
		environment = new Environment(width, height);
		environment.start(new Task2());

		stage = new BasicStage(environment);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void render () {
		float delta = Gdx.graphics.getDeltaTime();
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		environment.update(delta);
		environment.render();
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		environment.viewport.update(width, height);
		stage.getViewport().update(width, height);
	}
}
