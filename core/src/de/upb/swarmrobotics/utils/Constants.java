package de.upb.swarmrobotics.utils;

/**
 * Created on 31.05.2016.
 *
 * @author Henrik
 */
public class Constants {
    public static final int MIN_WIDTH = 800;
    public static final int MIN_HEIGHT = 450;
    public static final int PIXELS_PER_METER = 32;
    public static final float TIME_STEP = 1/300f;
    public static final int VELOCITY_ITERATIONS = 6;
    public static final int POSITION_ITERATIONS = 2;
}
