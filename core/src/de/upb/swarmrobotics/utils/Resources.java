package de.upb.swarmrobotics.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

/**
 * Created on 01.06.2016.
 *
 * @author Henrik
 */
public class Resources {
    public static Skin skin;

    public static void init() {
        skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
    }
}
