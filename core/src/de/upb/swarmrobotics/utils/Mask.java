package de.upb.swarmrobotics.utils;

/**
 * Created on 31.05.2016.
 *
 * @author Henrik
 */
public class Mask {
    public static final short WALL = 1;
    public static final short ROBOT = 2;
    public static final short SENSOR = 4;
    public static final short OBSTACLE = 8;

    public static final short SensorMask = WALL | ROBOT | OBSTACLE;
    public static final short RobotMask = WALL | ROBOT | OBSTACLE | SENSOR;
}
