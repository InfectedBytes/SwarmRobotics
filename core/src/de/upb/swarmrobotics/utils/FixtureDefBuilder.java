package de.upb.swarmrobotics.utils;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Utility class used to construct fixtures, it must be used in conjunction with a BodyBuilder.
 * <pre>
 * builder.fixture(
 *   builder.fixtureDefBuilder().boxShape(2, 2)
 * );
 * </pre>
 * @see BodyBuilder#fixtureDefBuilder()
 *
 * @author Henrik
 */
@SuppressWarnings("unused")
public class FixtureDefBuilder {
    FixtureDef fixtureDef;
    Object userData;

    /**
     * Must be used in conjunction with BodyBuilder
     */
    FixtureDefBuilder() { reset(); }

    public FixtureDefBuilder sensor() {
        fixtureDef.isSensor = true;
        return this;
    }

    public FixtureDefBuilder userData(Object obj) { userData = obj; return this; }

    public FixtureDefBuilder edgeShape(Vector2 a, Vector2 b) {
        EdgeShape shape = new EdgeShape();
        shape.set(a, b);
        fixtureDef.shape = shape;
        return this;
    }

    public FixtureDefBuilder boxShape(float hx, float hy) {
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(hx, hy);
        fixtureDef.shape = shape;
        return this;
    }

    public FixtureDefBuilder circleShape(float radius) {
        Shape shape = new CircleShape();
        shape.setRadius(radius);
        fixtureDef.shape = shape;
        return this;
    }

    /**
     * Creates a circle at a specific distance and angle from the center.
     * @param radius
     * @param angle
     * @param distance
     * @return
     */
    public FixtureDefBuilder circleShape(float radius, float angle, float distance) {
        if(distance == 0) return circleShape(radius);
        float x = MathUtils.cos(angle) * distance;
        float y = MathUtils.sin(angle) * distance;
        int n = 8; // arbitrary polygons must have between 3 and 8 vertices
        Vector2[] poly = new Vector2[n];
        for(int i=0; i<n; i++) {
            angle = MathUtils.PI2 * i / n;
            float px = MathUtils.cos(angle) * radius;
            float py = MathUtils.sin(angle) * radius;
            poly[i] = new Vector2(x + px, y + py);
        }
        return polygonShape(poly);
    }

    public FixtureDefBuilder polygonShape(Vector2[] vertices) {
        PolygonShape shape = new PolygonShape();
        shape.set(vertices);
        fixtureDef.shape = shape;
        return this;
    }

    public FixtureDefBuilder density(float density) {
        fixtureDef.density = density;
        return this;
    }

    public FixtureDefBuilder friction(float friction) {
        fixtureDef.friction = friction;
        return this;
    }

    public FixtureDefBuilder restitution(float restitution) {
        fixtureDef.restitution = restitution;
        return this;
    }

    public FixtureDefBuilder categoryBits(short categoryBits) {
        fixtureDef.filter.categoryBits = categoryBits;
        return this;
    }

    public FixtureDefBuilder maskBits(short maskBits) {
        fixtureDef.filter.maskBits = maskBits;
        return this;
    }

    private void reset() { fixtureDef = new FixtureDef(); userData = null; }

    public FixtureDef build() {
        FixtureDef fixtureDef = this.fixtureDef;
        reset();
        return fixtureDef;
    }
}
