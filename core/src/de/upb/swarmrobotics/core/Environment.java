package de.upb.swarmrobotics.core;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import de.upb.swarmrobotics.utils.Constants;
import de.upb.swarmrobotics.utils.BodyBuilder;
import de.upb.swarmrobotics.utils.Util;

/**
 * Created on 31.05.2016.
 *
 * @author Henrik
 */
public class Environment implements ContactListener {
    public final World world;
    public final ExtendViewport viewport;
    public final int width, height;
    private Array<Robot> robots = new Array<Robot>();
    private final Box2DDebugRenderer debugRenderer;
    private float accumulator = 0;
    private Task task;
    private boolean pause = true;
    public void pause() { pause = true; }
    public void unpause() { pause = false; }
    public void togglePause() { pause = !pause; }

    /**
     * @param width width of the environment in meters
     * @param height height of the environment in meters
     */
    public Environment(int width, int height) {
        this.width = width;
        this.height = height;
        world = new World(new Vector2(), true);
        viewport = new ExtendViewport(width, height);
        world.setContactListener(this);
        debugRenderer = new Box2DDebugRenderer();
        // add some walls
        int halfw = width / 2;
        int halfh = height / 2;
        Vector2 bottomLeft = new Vector2(-halfw, -halfh);
        Vector2 bottomRight = new Vector2(halfw, -halfh);
        Vector2 topLeft = new Vector2(-halfw, halfh);
        Vector2 topRight = new Vector2(halfw, halfh);
        addWall(bottomLeft, topLeft); addWall(bottomRight, topRight);
        addWall(bottomLeft, bottomRight); addWall(topLeft, topRight);
    }

    /**
     * Returns the combined projection and view matrix of the viewport
     * @return
     */
    public Matrix4 getMatrix() { return viewport.getCamera().combined; }

    /**
     * Initializes the task and afterwards all robots
     * @param task
     */
    public void start(Task task) {
        this.task = task;
        task.onCreate(this);
        for(Robot robot : robots) robot.build();
    }

    /**
     * Udates all robots and the physics world. If paused this method will do nothing
     * @param delta
     */
    public void update(float delta) {
        if(pause) return;
        for(Robot robot : robots) robot.update(delta);
        doPhysicsStep(delta);
    }

    /**
     * Renders the world and afterwards it calls task.render
     */
    public void render() {
        debugRenderer.render(world, getMatrix());
        task.render(getMatrix());
    }

    private void doPhysicsStep(float deltaTime) {
        float frameTime = Math.min(deltaTime, 0.25f);
        accumulator += frameTime;
        while (accumulator >= Constants.TIME_STEP) {
            world.step(Constants.TIME_STEP, Constants.VELOCITY_ITERATIONS, Constants.POSITION_ITERATIONS);
            accumulator -= Constants.TIME_STEP;
        }
    }

    private void addWall(Vector2 a, Vector2 b) {
        BodyBuilder builder = new BodyBuilder(world);
        builder.type(BodyDef.BodyType.StaticBody).fixture(
                builder.fixtureDefBuilder().edgeShape(a, b)
        ).build();
    }

    /**
     * Creates a new robot with the given radius and adds it to this environment
     * @param radius
     * @return
     */
    public Robot createRobot(float radius) {
        Robot robot = new Robot(this, radius);
        robots.add(robot);
        return robot;
    }

    /**
     * Returns a new body builder.
     * @param dynamic true if the body should be a dynamic body otherwise it will be a static body
     * @return
     */
    public BodyBuilder bodyBuilder(boolean dynamic) {
        return new BodyBuilder(world).type(dynamic ? BodyDef.BodyType.DynamicBody : BodyDef.BodyType.StaticBody);
    }

    @Override
    public void beginContact(Contact contact) {
        Fixture a = contact.getFixtureA();
        Fixture b = contact.getFixtureB();
        if(Util.isSensor(a)) ((FixtureSensor)a.getUserData()).beginContact(b);
        if(Util.isSensor(b)) ((FixtureSensor)b.getUserData()).beginContact(a);
    }

    @Override
    public void endContact(Contact contact) {
        Fixture a = contact.getFixtureA();
        Fixture b = contact.getFixtureB();
        if(Util.isSensor(a)) ((FixtureSensor)a.getUserData()).endContact(b);
        if(Util.isSensor(b)) ((FixtureSensor)b.getUserData()).endContact(a);
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {}

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) { }
}
