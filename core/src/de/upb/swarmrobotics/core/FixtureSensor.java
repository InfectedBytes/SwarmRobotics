package de.upb.swarmrobotics.core;

import com.badlogic.gdx.physics.box2d.Fixture;

/**
 * Base class physics sensors.
 * It has a counter that is incremented whenever a collision happens and decremented when it dissolves.
 *
 * @author Henrik
 */
public abstract class FixtureSensor extends Sensor {
    public int collisions;
    public boolean isTriggered() { return collisions > 0; }
    public void beginContact(Fixture other) {
        collisions++;
    }
    public void endContact(Fixture other) {
        collisions--;
    }
}
