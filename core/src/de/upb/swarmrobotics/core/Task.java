package de.upb.swarmrobotics.core;

import com.badlogic.gdx.math.Matrix4;

/**
 * Created on 31.05.2016.
 *
 * @author Henrik
 */
public interface Task {
    void onCreate(Environment environment);
    void render(Matrix4 mvp);
}
