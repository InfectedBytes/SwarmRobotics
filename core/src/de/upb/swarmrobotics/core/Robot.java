package de.upb.swarmrobotics.core;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;
import de.upb.swarmrobotics.utils.BodyBuilder;
import de.upb.swarmrobotics.utils.Mask;

/**
 * Robot with physics body and different sensors. Should be created from the environment with env.createRobot(radius)
 *
 * @author Henrik
 */
public class Robot {
    public final BodyBuilder builder;
    private Array<Sensor> sensors = new Array<Sensor>();
    private Behavior behavior;
    private Body body;
    private float targetSpeed;
    private Vector2 initPos;
    private float initAngle;

    Robot(Environment environment, float radius) {
        builder = environment.bodyBuilder(true).userData(this);
        builder.fixture(builder.fixtureDefBuilder().circleShape(radius) // fixture is of a circleShape
                .maskBits(Mask.RobotMask) // what the fixture can collide with
                .categoryBits(Mask.ROBOT) // what the fixture itself is
                .userData(this));
    }

    /**
     * Actually builds the physics body, should only be called once from inside the environment.
     */
    void build() {
        body = builder.build();
        setTransform(initPos, initAngle);
    }

    /**
     * Returns the i-th sensor
     *
     * @param i
     * @return
     */
    public Sensor sensor(int i) { return sensors.get(i); }

    /**
     * Returns the first sensor of the given class
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public <T extends Sensor> T sensor(Class<T> clazz) {
        for(Sensor sensor : sensors) {
            if(sensor.getClass() == clazz) return (T)sensor;
        }
        return null;
    }

    /**
     * Returns the first sensor of the given class and at the given position
     *
     * @param clazz
     * @param position
     * @param <T>
     * @return
     */
    public <T extends Sensor> T sensor(Class<T> clazz, SensorPosition position) {
        for(Sensor sensor : sensors)
            if(sensor.getClass() == clazz && sensor.position == position) return (T)sensor;
        return null;
    }

    /**
     * Attaches the given sensor to this robot and calls sensor.attachedTo
     * @param sensor
     */
    public void attach(Sensor sensor) {
        sensors.add(sensor);
        sensor.attachedTo(this);
    }

    public void initialPosition(float x, float y) { initPos = new Vector2(x, y);}

    public void initialPosition(Vector2 pos) { initPos = pos;}

    public void initialAngle(float angle) { initAngle = angle; }

    /**
     * Sets the given behavior and calls the behaviors onCreate method.
     * This method should be called only if the rest of the robot is already set up.
     * @param behavior
     */
    public void attach(Behavior behavior) {
        this.behavior = behavior;
        behavior.setRobot(this);
        behavior.onCreate();
    }

    public void setTransform(Vector2 pos, float angle) { setTransform(pos.x, pos.y, angle); }

    public void setTransform(float x, float y, float angle) {
        body.setTransform(x, y, angle);
        body.setAwake(true);
    }

    public void setPosition(Vector2 pos) { setPosition(pos.x, pos.y); }

    public void setPosition(float x, float y) { setTransform(x, y, body.getAngle()); }

    public Vector2 getPosition() { return body.getPosition(); }

    public void setAngle(float angle) {
        Vector2 pos = getPosition();
        setTransform(pos.x, pos.y, angle);
    }

    public float getAngle() { return body.getAngle(); }

    public void turnDeg(float angle) { turn(MathUtils.degRad * angle); }

    public void turn(float angle) {
        Vector2 pos = body.getPosition();
        angle += body.getAngle();
        setTransform(pos.x, pos.y, angle);
    }

    public void setTargetSpeed(float v) { targetSpeed = v; }

    public float getTargetSpeed() { return targetSpeed; }

    public void update(float delta) {
        behavior.update(delta);
        float angle = body.getAngle();
        float dx = MathUtils.cos(angle) * targetSpeed;
        float dy = MathUtils.sin(angle) * targetSpeed;
        body.setLinearVelocity(dx, dy);
    }
}
