package de.upb.swarmrobotics.core;

/**
 * Created on 31.05.2016.
 *
 * @author Henrik
 */
public abstract class Sensor {
    public SensorPosition position = SensorPosition.Center;
    public abstract void attachedTo(Robot robot);
}
