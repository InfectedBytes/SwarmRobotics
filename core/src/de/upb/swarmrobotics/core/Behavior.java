package de.upb.swarmrobotics.core;

/**
 * Created on 31.05.2016.
 *
 * @author Henrik
 */
public class Behavior {
    protected Robot robot;

    /**
     * Called when this behavior is attache to a robot: {@link Robot#attach(Behavior)}
     * @param robot
     */
    void setRobot(Robot robot) { this.robot = robot; }

    /**
     * Called when this behavior is attached to a robot: {@link Robot#attach(Behavior)}
     */
    public void onCreate() {

    }

    /**
     * Called every frame from inside {@link Robot#update(float)}
     * @param delta
     */
    public void update(float delta) {

    }
}
