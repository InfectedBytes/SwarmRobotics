package de.upb.swarmrobotics.core;

import com.badlogic.gdx.math.MathUtils;

/**
 * @author Henrik
 */
public enum SensorPosition {
    FrontLeft(45), Front(0), FrontRight(-45),
    Left(90), Center(0), Right(-90),
    BackLeft(135), Back(180), BackRight(-135),
    Other(0); // hacky special position

    public final float angle;
    SensorPosition(float angle) { this.angle = angle; }
    SensorPosition(int degree) { this(MathUtils.degRad * degree); }
}
