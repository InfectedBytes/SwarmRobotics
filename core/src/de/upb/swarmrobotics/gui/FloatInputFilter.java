package de.upb.swarmrobotics.gui;

import com.badlogic.gdx.scenes.scene2d.ui.TextField;

/**
 * Created on 03.06.2016.
 *
 * @author Henrik
 */
public class FloatInputFilter implements TextField.TextFieldFilter {
    public final static FloatInputFilter instance = new FloatInputFilter();

    @Override
    public boolean acceptChar(TextField textField, char c) {
        return Character.isDigit(c) || c == '.';
    }
}
