package de.upb.swarmrobotics.sheet3;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import de.upb.swarmrobotics.core.Environment;
import de.upb.swarmrobotics.core.Robot;
import de.upb.swarmrobotics.core.SensorPosition;
import de.upb.swarmrobotics.core.Task;
import de.upb.swarmrobotics.sensors.Bumper;
import de.upb.swarmrobotics.sensors.RobotDetector;

/**
 * Created on 01.06.2016.
 *
 * @author Henrik
 */
public class Task2 implements Task {
    @Override
    public void onCreate(Environment environment) {
        final int halfW = environment.width / 2 - 1;
        final int halfH = environment.height / 2 - 1;
        final int n = 20;
        for(int i=0; i<n; i++) {
            float x = MathUtils.random(-halfW, halfW);
            float y = MathUtils.random(-halfH, halfH);
            float angle = MathUtils.random(0, MathUtils.PI2);
            placeRobot(environment, x, y, angle);
        }
    }

    private void placeRobot(Environment environment, float x, float y, float angle) {
        Robot robot = environment.createRobot(0.5f);
        robot.attach(new RobotDetector(0.75f, 0, SensorPosition.Center));
        robot.attach(new Bumper(0.25f, 0.5f, SensorPosition.FrontLeft));
        robot.attach(new Bumper(0.25f, 0.5f, SensorPosition.Front));
        robot.attach(new Bumper(0.25f, 0.5f, SensorPosition.FrontRight));
        robot.initialPosition(x, y);
        robot.initialAngle(angle);
        robot.attach(new StoppingSwarm());
    }

    @Override
    public void render(Matrix4 mvp) {

    }
}