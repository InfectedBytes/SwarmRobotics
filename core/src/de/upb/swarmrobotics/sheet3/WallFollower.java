package de.upb.swarmrobotics.sheet3;

import de.upb.swarmrobotics.core.Behavior;
import de.upb.swarmrobotics.core.SensorPosition;
import de.upb.swarmrobotics.sensors.Bumper;

/**
 * Created on 01.06.2016.
 *
 * @author Henrik
 */
public class WallFollower extends Behavior {
    private Bumper wall, left, front, right;
    private enum State { FindWall, TurnLeft, TurnRight, Follow}
    private State state = State.FindWall;

    @Override
    public void onCreate() {
        wall = robot.sensor(Bumper.class, SensorPosition.Other);
        left = robot.sensor(Bumper.class, SensorPosition.FrontLeft);
        front = robot.sensor(Bumper.class, SensorPosition.Front);
        right = robot.sensor(Bumper.class, SensorPosition.FrontRight);
    }

    @Override
    public void update(float delta) {
        boolean left = this.left.isTriggered();
        boolean front = this.front.isTriggered();
        boolean right = this.right.isTriggered();
        boolean wall = this.wall.isTriggered();

        if(state == State.FindWall) {
            robot.setTargetSpeed(5);
            if(left || front || right || wall) state = State.TurnLeft;
        }
        if(state == State.TurnLeft) {
            robot.setTargetSpeed(0);
            if(!wall || front || right) robot.turnDeg(1);
            else state = State.Follow;
        } else if(state == State.TurnRight) {
            robot.setTargetSpeed(0);
            if(!wall) robot.turnDeg(-5);
            else state = State.Follow;
        }
        if(state == State.Follow) {
            robot.setTargetSpeed(3);
            if(front || right) state = State.TurnLeft;
            else if(left || !wall) state = State.TurnRight;
        }
    }
}
