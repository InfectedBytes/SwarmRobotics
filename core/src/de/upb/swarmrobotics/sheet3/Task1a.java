package de.upb.swarmrobotics.sheet3;

import com.badlogic.gdx.math.Matrix4;
import de.upb.swarmrobotics.core.Environment;
import de.upb.swarmrobotics.core.Robot;
import de.upb.swarmrobotics.core.SensorPosition;
import de.upb.swarmrobotics.core.Task;
import de.upb.swarmrobotics.sensors.Bumper;

/**
 * Created on 31.05.2016.
 *
 * @author Henrik
 */
public class Task1a implements Task {
    @Override
    public void onCreate(Environment environment) {
        Robot robot = environment.createRobot(0.5f);
        robot.attach(new Bumper(0.25f, 0.5f, SensorPosition.FrontLeft));
        robot.attach(new Bumper(0.25f, 0.5f, SensorPosition.Front));
        robot.attach(new Bumper(0.25f, 0.5f, SensorPosition.FrontRight));
        robot.initialPosition(5, 0);
        robot.attach(new CollisionAvoidance());
    }

    @Override
    public void render(Matrix4 mvp) {

    }
}
