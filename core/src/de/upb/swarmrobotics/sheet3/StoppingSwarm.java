package de.upb.swarmrobotics.sheet3;

import com.badlogic.gdx.math.MathUtils;
import de.upb.swarmrobotics.core.Behavior;
import de.upb.swarmrobotics.core.SensorPosition;
import de.upb.swarmrobotics.sensors.Bumper;
import de.upb.swarmrobotics.sensors.RobotDetector;

/**
 * Created on 01.06.2016.
 *
 * @author Henrik
 */
public class StoppingSwarm extends Behavior {
    private final float WaitCoolDown = 5;
    private final float IgnoreCoolDown = 0.5f;
    private Bumper left, front, right;
    private RobotDetector detector;

    private enum State { Move, Wait, Turn }
    private State state = State.Move;
    private float cooldown;
    private boolean ignoreRobots;

    @Override
    public void onCreate() {
        detector = robot.sensor(RobotDetector.class);
        left = robot.sensor(Bumper.class, SensorPosition.FrontLeft);
        front = robot.sensor(Bumper.class, SensorPosition.Front);
        right = robot.sensor(Bumper.class, SensorPosition.FrontRight);
    }

    @Override
    public void update(float delta) {
        cooldown -= delta;
        boolean detector = this.detector.isTriggered();
        boolean left = this.left.isTriggered();
        boolean front = this.front.isTriggered();
        boolean right = this.right.isTriggered();

        robot.setTargetSpeed(0);
        if(state == State.Move) {
            if(ignoreRobots && cooldown <= 0) ignoreRobots = false;
            if(!ignoreRobots && detector) {
                state = State.Wait;
                cooldown = WaitCoolDown + MathUtils.random(-1, 1) + this.detector.collisions * this.detector.collisions;
            } else if(left || front || right) {
                state = State.Turn;
                robot.turnDeg(MathUtils.random(-45, 45));
            } else {
                robot.setTargetSpeed(5);
            }
        }
        if(state == State.Wait && (cooldown <= 0 || !detector)) {
            state = State.Turn;
            ignoreRobots = true;
            cooldown = IgnoreCoolDown;
        }
        if(state == State.Turn) {
            if(left || front || right)
                robot.turnDeg(5);
            else
                state = State.Move;
        }
    }
}
