package de.upb.swarmrobotics.sensors;

import com.badlogic.gdx.physics.box2d.Fixture;
import de.upb.swarmrobotics.core.Robot;
import de.upb.swarmrobotics.core.SensorPosition;

/**
 * Bumper that only count collisions with other robots
 *
 * @author Henrik
 */
public class RobotDetector extends Bumper {
    public RobotDetector(float radius, float distance, float angle) {
        super(radius, distance, SensorPosition.Other);
    }
    public RobotDetector(float radius, float distance, SensorPosition position) {
        super(radius, distance, position);
    }

    @Override
    public void beginContact(Fixture other) {
        if(other.getBody().getUserData() instanceof Robot)
            super.beginContact(other);
    }

    @Override
    public void endContact(Fixture other) {
        if(other.getBody().getUserData() instanceof Robot)
            super.endContact(other);
    }
}
