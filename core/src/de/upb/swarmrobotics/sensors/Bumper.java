package de.upb.swarmrobotics.sensors;

import com.badlogic.gdx.math.MathUtils;
import de.upb.swarmrobotics.core.FixtureSensor;
import de.upb.swarmrobotics.utils.Mask;
import de.upb.swarmrobotics.core.Robot;
import de.upb.swarmrobotics.core.SensorPosition;
import de.upb.swarmrobotics.utils.BodyBuilder;

/**
 * Created on 31.05.2016.
 *
 * @author Henrik
 */
public class Bumper extends FixtureSensor {
    private float radius;
    private float angle;
    private float distance;

    /**
     * Creates a Bumper at an arbitrary angle, setting the position to SensorPosition.Other
     * @param radius
     * @param distance
     * @param angle
     */
    public Bumper(float radius, float distance, float angle) {
        this(radius, distance, SensorPosition.Other);
        this.angle = MathUtils.degRad * angle;
    }

    /**
     * Creates a bumper at the given position
     * @param radius
     * @param distance
     * @param position
     */
    public Bumper(float radius, float distance, SensorPosition position) {
        this.position = position;
        this.radius = radius;
        this.angle = position.angle;
        this.distance = distance;
    }

    @Override
    public void attachedTo(Robot robot) {
        BodyBuilder builder = robot.builder;
        builder.fixture(builder.fixtureDefBuilder().sensor().maskBits(Mask.SensorMask).categoryBits(Mask.SENSOR)
                .circleShape(radius, angle, distance).userData(this));
    }
}
